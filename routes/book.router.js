const express = require("express")
const router = express.Router()

const bookController = require('../controllers/book.controller')
const tamuController = require('../controllers/tamu.controller')

router.get("/guests/", bookController.getAll)
router.get("/guests/:id", bookController.getById)
router.post("/guests/", bookController.create)
router.put("/guests/:id", bookController.updateById)
router.delete("/guests/:id", bookController.deleteById)


router.get("/tamu/", tamuController.getAll)
router.get("/tamu/:id", tamuController.getById)
router.post("/tamu/", tamuController.create)
router.put("/tamu/:id", tamuController.updateById)
router.delete("/tamu/:id", tamuController.deleteById)

module.exports = router